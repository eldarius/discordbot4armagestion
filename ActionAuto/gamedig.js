const Gamedig = require('gamedig');
const client = require("../index");

StatusGame = async () => Gamedig.query({
  type: client.config.gameName,
  host: client.config.gameIp
}).then((state) => {
  console.log("Le serveur '" + state.name + "' est EN LIGNE, \nMap :" + state.map + "\nNombre de joueur connecté : " + state.raw.numplayers);
  return "Le serveur '" + state.name + "' est EN LIGNE, \nMap :" + state.map + "\nNombre de joueur connecté : " + state.raw.numplayers;
}).catch((error) => {
  console.log("Server is offline");
  return "Serveur is offline";
});

module.exports = {
  name: "refresh",
  async execute() {
    statut = await StatusGame();
    return statut;
  },
};