const Rcon = require('rcon');
const client = require('../index');

function connectRcon() {
  return new Promise((resolve, reject) => {
    const conn = new Rcon(client.config.gameIp, client.config.rconPort, client.config.rconPassword);
    conn.connect();

    conn.on('auth', () => {
      console.log("Authenticated");
      console.log("Sending command: manage players");
      conn.send("manage players");
    }).on('response', (str) => {
      console.log("Response: " + str);
      conn.disconnect();
      resolve(str);
    }).on('error', (err) => {
      console.log("Error: " + err);
      conn.disconnect();
      reject(err);
    }).on('end', () => {
      console.log("Connection closed");
    });
  });
}

async function getStatus() {
  try {
    const status = await connectRcon();
    return `Le serveur ECO est EN LIGNE, \nJoueurs ${status}`;
  } catch (error) {
    throw new Error(`Serveur is offline \nErreur -> ${error}`);
  }
}

module.exports = {
  name: "refresh",
  async execute() {
    try {
      const status = await getStatus();
      return status;
    } catch (error) {
      console.error("Erreur lors de la récupération du statut :", error);
      return "Erreur lors de la récupération du statut.";
    }
  },
};