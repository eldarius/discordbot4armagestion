const { Client, Interaction } = require("discord.js");
const { editMessage } = require("../embed/MessageEmbed.js");
const child_process = require('child_process');
const client = require('../index');
const jsonFile = require("../config.json");

//===============================START SERVER================================
function StartServer(execGame) {
    editMessage({ titre: "serveur en cours de lancement" })
    return child_process.exec(execGame,
        function (error, stdout, stderr) {
            console.log("erreur :" + error);
            if (error) editMessage({ titre: "Erreur de lancement", description: error.toString() })
            console.log("stdout :" + stdout);
            if (stdout) editMessage({ titre: "serveur en cours de lancement" })
            console.log("stderr :" + stderr);
        });

}
//======================================/////==========================================

module.exports = {
    name: "start",
    /**
     * 
     * @param {Interaction} interaction 
     * @param {Client} client 
     * @param {String[]} args
     * @returns 
     */
    run: async (interaction, Client, args) => {
        
        console.log("START !!!!!");
        application = StartServer(client.config.start);
        console.log(application);
    }
};