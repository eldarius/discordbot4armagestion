const {Client, Interaction } = require("discord.js");
const {editMessage} = require("../embed/MessageEmbed.js");
const client = require('../index');

async function stopServer(execToKill) {
    const find = require('find-process');
    var kill = require('tree-kill');

    find('name', execToKill, true)
    .then(function (list) {
        list.forEach(element => {
            console.log("=STOP=",element.name +"\n"+ element.pid)
            kill(element.pid, 'SIGHUP');
        });
    });
    editMessage({titre:'serveur '+execToKill+' coupé'})
}

module.exports = {
	name: "stop",
    /**
     * 
     * @param {*} interaction 
     * @param {*} client 
     * @param {*} execName 
     * @returns 
     */
	run : async(interaction, client, execName) =>{
        console.log("STOP");
		await stopServer(client.config.stop);
	},
};