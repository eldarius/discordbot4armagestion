const { EmbedBuilder, ActionRowBuilder, ButtonBuilder } = require('discord.js');
const client = require("../index");

function myComponents({ showStart, showStop }) {
	if (showStart || showStop) {
		const buttons = new ActionRowBuilder();
		if (showStart) {
			buttons.addComponents(
				new ButtonBuilder()
					.setCustomId('start')
					.setLabel('START ECO')
					.setStyle('Success'),
			);
		}
		if (showStop) {
			buttons.addComponents(
				new ButtonBuilder()
					.setCustomId('stopArmaServers')
					.setLabel('STOP ECO')
					.setStyle('Danger'),
			);
		}
		return [buttons]
	} else {
		return false
	}
}

function myEmbed({ titre, description = " ", fieldTitle = "titrefield", FieldValue = "notefield" }) {
	return Embed = new EmbedBuilder()
		//.setColor('#0099ff')
		.setTitle(titre)
		//.setURL('https://discord.js.org/')
		//.setAuthor({ name: 'Some name', iconURL: 'https://i.imgur.com/AfFp7pu.png', url: 'https://discord.js.org' })
		.setDescription(description)
		//.setThumbnail('https://i.imgur.com/AfFp7pu.png')
		/*.addFields(
			{ name: 'Regular field title', value: 'Some value here' },
			{ name: '\u200B', value: '\u200B' },
			{ name: 'Inline field title', value: 'Some value here', inline: true },
			{ name: 'Inline field title', value: 'Some value here', inline: true },
		)*/
		//+++++++.addField(fieldTitle, FieldValue, true)
		//.setImage('https://i.imgur.com/AfFp7pu.png')
		.setTimestamp()
	//.setFooter({ text: 'Some footer text here', iconURL: 'https://i.imgur.com/AfFp7pu.png' });
}

function embedMessage({ titre, description, fieldTitle, FieldValue, showStart = true, showStop = true }) {

	const resultEmbed = myEmbed({ titre: titre, description: description, fieldTitle: fieldTitle, FieldValue: FieldValue })
	const resultComponents = myComponents({ showStart: showStart, showStop: showStop })

	if (resultComponents) {
		return { embeds: [resultEmbed], components: resultComponents }
	} else {
		return { embeds: [resultEmbed]}
	}
	
}

exports.editMessage = async ({ titre, description}) => {
	msg = await client.channels.cache.get(client.config.channelId).messages.fetch(client.config.messageId);
	msg.edit(embedMessage({ titre: titre, description: description, showStart: false, showStop: false }))
	if (client.config.channelAdminId != "") {
		msgAdmin = await client.channels.cache.get(client.config.channelAdminId).messages.fetch(client.config.messageAdminId);
		msgAdmin.edit(embedMessage({titre : titre, description : description}))
	}
}