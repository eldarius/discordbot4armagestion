const client = require("../index");

client.on("interactionCreate", async (interaction) => {
    console.log("interaction");
    console.log(interaction);
    // Slash Command Handling
    if (interaction.isCommand()) {
        await interaction.deferReply({ ephemeral: false }).catch(() => {});

        const cmd = client.slashCommands.get(interaction.commandName);
        if (!cmd)
            return interaction.followUp({ content: "An error has occurred." });

        const args = [];

        for (let option of interaction.options.data) {
            if (option.type === "SUB_COMMAND") {
                if (option.name) args.push(option.name);
                option.options?.forEach((x) => {
                    if (x.value) args.push(x.value);
                });
            } else if (option.value) args.push(option.value);
        }

        interaction.member = interaction.guild.members.cache.get(interaction.user.id);

        cmd.run(client, interaction, args);
    }

    //buttons
    if (interaction.isButton()) {
        await interaction.deferUpdate();
        const command = client.buttons.get(interaction.customId);
        if (command) command.run(client, interaction);
    }
<<<<<<< HEAD
});
