const client = require("../index");
const fs = require('node:fs');
const { editMessage } = require("../embed/MessageEmbed.js");
const timeBtwnRefresh = client.config.timeBtwnRefresh;

async function RefreshInfo() {
  const command = require(`../ActionAuto/refreshServeurStatus.js`);
  messageRetour = await command.execute();
  editMessage({ titre: messageRetour });
  await sleep(timeBtwnRefresh * 1000);
  RefreshInfo(timeBtwnRefresh);
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

client.on("ready", async () => {
  //on verifie sur l'id du message est déja sauvegardé

  const jsonFile = require("../config.json");

  console.log("OnReady => new message");

  if (client.config.messageId == "" ||  client.config.messageId == null) {
    message = await client.channels.cache.get(client.config.channelId).send("DB4ServeurG");
    jsonFile.messageId = message.id;
  }
  if ((client.config.messageAdminId == "" ||  client.config.messageId == null) && client.config.channelAdminId != "") {
    messageAdmin = await client.channels.cache.get(client.config.channelAdminId).send("DB4ServeurG");
    jsonFile.messageAdminId = messageAdmin.id;
  }

  fs.writeFile("config.json", JSON.stringify(jsonFile, null, 2), function (err, result) {
    if (err) console.log('error writeFile config.json =>', err);
  });


  console.log(`${client.user.tag} is up and ready to go!`)
  editMessage({ titre: client.user.tag + ' is up and ready to go!' })
  sleep(5000);
  RefreshInfo();
});