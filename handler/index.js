const { glob } = require("glob");
const { promisify } = require("util");
const { Client } = require("discord.js");

const globPromise = promisify(glob);

/**
 * Initialise les boutons et les événements pour le bot Discord.
 * 
 * @param {Client} client - L'instance du client Discord.
 */
module.exports = async (client) => {
    // Boutons
    const processPath = process.cwd().replace(/\\/g, '/')
    const buttonsFiles = await globPromise(`${processPath}/buttons/*.js`);
    buttonsFiles.map((value) => {
        const file = require(value);
        const splitted = value.split("/");
        const directory = splitted[splitted.length - 2];

        if (file.name) {
            const properties = { directory, ...file };
            client.buttons.set(file.name, properties);
        }
    });

    // Events
    const eventFiles = await globPromise(`${processPath}/events/*.js`);
    eventFiles.map((value) => require(value));
};